\documentclass[a4paper]{article}

\usepackage{INTERSPEECH_v2}

% watermark, comment out
% \usepackage{draftwatermark}
% \SetWatermarkText{DRAFT}
% \SetWatermarkScale{1}

\linespread{0.915}

% for URLs
\usepackage{url}

% graphics path
\graphicspath{{figures/}}

% For IPA symbols
\usepackage{tipa}

% For scientific units
\usepackage{siunitx}

% subfigure organization
\usepackage[labelfont=bf,labelsep=quad]{caption}

% captions for subfigures
\usepackage[position=t,singlelinecheck=off]{subcaption}
\usepackage{xcolor}
\DeclareCaptionFont{white}{\color{white}}
\DeclareCaptionFont{black}{\color{black}}
\DeclareCaptionLabelFormat{bold}{\textbf{#2}}
\captionsetup[sub]{ % The subcaption settings
    labelfont=white,
    subrefformat=bold}        % Use our custom label format

% macro for placing subfigure caption inside subfigure
\newcommand{\subfigimg}[3][,]{%
  \setbox1=\hbox{\includegraphics[#1]{#3}}% Store image in box
  \leavevmode\rlap{\usebox1}% Print image
  \rlap{\hspace*{10pt}\raisebox{\dimexpr\ht1-2\baselineskip}{\caption{\label{#2}}}}% Print label
  \phantom{\usebox1}% Insert appropriate spcing
}

% for white font in subfigure captions
\usepackage{xcolor}

% multiple row cells
\usepackage{multirow,array}
\newcommand\T{\rule{0pt}{2.6ex}}       % Top strut
\newcommand\B{\rule[-1.2ex]{0pt}{0pt}} % Bottom strut
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}

\setlength{\textfloatsep}{0.2cm}
\setlength{\dbltextfloatsep}{0cm}

\title{Test-retest repeatability of articulatory strategies\\ using real-time magnetic resonance imaging}
\name{Tanner Sorensen$^{12}$, Asterios Toutios$^1$, Johannes T{\"o}ger$^1$\sthanks{\hspace{0.15cm} now at Lund University}, Louis Goldstein$^2$, Shrikanth Narayanan$^1$}
\address{
$^1$Signal Analysis and Interpretation Lab, University of Southern California, Los Angeles, CA, USA\\ 
$^2$Department of Linguistics, University of Southern California, Los Angeles, CA, USA}
\email{tsorense@usc.edu}

% I didn't use epiglottis for cd measurement, right? I didn't use it for the tongue factor, so it should not be used for cd measurement.
% Make sure that the order is always written "jaw, lips, tongue".
% Cite that chapter of the book on the pseudo inverse. Give the page numbers.

\begin{document}

\maketitle

\begin{abstract}
Real-time magnetic resonance imaging (rtMRI) provides information about the dynamic shaping of the vocal tract during speech production. This paper introduces and evaluates a method for quantifying articulatory strategies using rtMRI. The method decomposes the formation and release of a constriction in the vocal tract into the contributions of individual articulators such as the jaw, tongue, lips, and velum. The method uses an anatomically guided factor analysis and dynamical principles from the framework of Task Dynamics. We evaluated the method within a test-retest repeatability framework. We imaged healthy volunteers ($n=8$, 4 females, 4 males) in two scans on the same day and quantified inter-study agreement with the intraclass correlation coefficient and mean within-subject standard deviation. The evaluation established a limit on effect size and intra-group differences in articulatory strategy which can be studied using the method. 
\end{abstract}
\noindent\textbf{Index Terms}: speech production, magnetic resonance imaging

\section{Introduction}

The vocal tract produces speech sounds using a flexible combination of speech articulators. Just as a pointing movement of the limb can be achieved with an infinite number of joint angles, so too does a speech task prescribe no unique role for any one articulator. An \textit{articulatory strategy} characterizes the way the articulators move to perform a speech task out of the many ways possible. The range of articulatory strategies used by a speaker indicates flexibility in motor organization~\cite{latash2012bliss}, which underlies variability in articulation and the acoustic signal. 

This study introduces and evaluates a \textit{quantitative imaging biomarker} of articulatory strategies as an indicator of flexibility in speech motor organization. In line with recent standardization in biomedical imaging, a quantitative imaging biomarker is ``an objective characteristic derived from an in vivo image measured on a ratio or interval scale as an indicator of normal biological processes, pathogenic processes or a response to a therapeutic intervention''~\cite{kessler2015emerging,sullivan2015metrology}. 
Quantitative imaging biomarkers are designed to extract complex information from biomedical images using mathematical algorithms. 
The proliferation of vocal tract imaging databases~\cite{narayanan2014real} and the morphological~\cite{lammert2013morphological} and functional~\cite{dawson2016methods} complexities captured therein underscore the importance of quantitative imaging biomarkers in speech science. 

Advances in real-time magnetic resonance imaging (rtMRI) have achieved a balance among the competing factors of temporal resolution, spatial resolution, and signal-to-noise ratio that allows for the characterization of vocal tract shaping during speech production~\cite{toutios2016advances,lingala2016state}. Alongside these advances in acquisition and reconstruction have grown computational approaches to extract quantitative imaging biomarkers from rtMRI~\cite{toger2017test}. Increasingly complex computational methods promise to provide biomarkers of articulatory strategies~\cite{Sorensen+2016}. However, a necessary preliminary to increasing the complexity of computational methods is evaluation of biomarker \textit{precision}. The precision of a quantitative imaging biomarker is the agreement between replicate measurements of the same or similar experimental units with specified conditions~\cite{kessler2015emerging,sullivan2015metrology}. Precision is an important parameter, as it establishes a limit on effect size and intra-group differences which can be studied using the method.

The goals of this study were (i) to introduce a quantitative imaging biomarker of articulatory strategies, and (ii) to evaluate the precision of the articulatory strategy biomarker within a test-retest framework. Files for repeating and replicating this study are available at \url{http://sail.usc.edu/span/artstr}. 

\section{Methods}

\begin{figure*}%
	\centering
    \begin{tabular}{C{0.14\textwidth} C{0.14\textwidth} C{0.14\textwidth} C{0.14\textwidth} C{0.14\textwidth} C{0.14\textwidth}}
    \SI{0}{\milli\second} & \SI{36}{\milli\second} & \SI{60}{\milli\second} & \SI{96}{\milli\second} & \SI{120}{\milli\second} & \SI{156}{\milli\second}
    \end{tabular}
	\begin{subfigure}{\textwidth}%
		\includegraphics[width=0.166\linewidth]{a1.png}\llap{
      \parbox[b]{0.166\textwidth}{\caption{\vspace{-2ex}\label{fig:transition}\textnormal{\textbf{\color{white}{rtMRI video}}}\rule{0ex}{-0.1\textwidth}}}}%
		\includegraphics[width=0.166\linewidth]{a2.png}%
		\includegraphics[width=0.166\linewidth]{a3.png}%
		\includegraphics[width=0.166\linewidth]{a4.png}%
		\includegraphics[width=0.166\linewidth]{a5.png}%
		\includegraphics[width=0.166\linewidth]{a6.png}%
	\end{subfigure}%
	\newline
    \vspace{0.1ex}
	\begin{subfigure}{\textwidth}%
		\includegraphics[width=0.166\textwidth]{a1_contours.png}\llap{
      \parbox[b]{0.166\textwidth}{\caption{\vspace{-2ex}\label{fig:contours}\textnormal{\textbf{\color{white}{contours}}\rule{0ex}{-0.1\textwidth}}}}}%
		\includegraphics[width=0.166\textwidth]{a2_contours.png}%
		\includegraphics[width=0.166\textwidth]{a3_contours.png}%
		\includegraphics[width=0.166\textwidth]{a4_contours.png}%
		\includegraphics[width=0.166\textwidth]{a5_contours.png}%
		\includegraphics[width=0.166\textwidth]{a6_contours.png}%
	\end{subfigure}
	\newline
    \vspace{0.1ex}
	\begin{subfigure}{\textwidth}%
		\includegraphics[width=0.166\textwidth]{a1_cd.png}\llap{
      \parbox[b]{0.166\textwidth}{\caption{\vspace{-2ex}\label{fig:cd}\textnormal{\textbf{\color{white}{constriction}}\rule{0ex}{-0.1\textwidth}}}}}%
		\includegraphics[width=0.166\textwidth]{a2_cd.png}%
		\includegraphics[width=0.166\textwidth]{a3_cd.png}%
		\includegraphics[width=0.166\textwidth]{a4_cd.png}%
		\includegraphics[width=0.166\textwidth]{a5_cd.png}%
		\includegraphics[width=0.166\textwidth]{a6_cd.png}%
	\end{subfigure}
	\newline
    \vspace{0.1ex}
    \begin{subfigure}{\textwidth}
      \includegraphics[width=0.166\textwidth]{a1_j}\llap{
      \parbox[b]{0.166\textwidth}{\caption{\vspace{-2ex}\label{fig:j}\textnormal{\textbf{\color{white}{jaw}}\rule{0ex}{-0.1\textwidth}}}}}%
      \includegraphics[width=0.166\textwidth]{a2_j}%
      \includegraphics[width=0.166\textwidth]{a3_j}%
      \includegraphics[width=0.166\textwidth]{a4_j}%
      \includegraphics[width=0.166\textwidth]{a5_j}%
      \includegraphics[width=0.166\textwidth]{a6_j}
    \end{subfigure}
	\newline
    \vspace{0.1ex}
    \begin{subfigure}{\textwidth}
      \includegraphics[width=0.166\textwidth]{a1_t}\llap{
      \parbox[b]{0.166\textwidth}{\caption{\vspace{-2ex}\label{fig:t}\textnormal{\textbf{\color{white}{tongue}}}}\rule{0ex}{-0.1\textwidth}}}%
      \includegraphics[width=0.166\textwidth]{a2_t}%
      \includegraphics[width=0.166\textwidth]{a3_t}%
      \includegraphics[width=0.166\textwidth]{a4_t}%
      \includegraphics[width=0.166\textwidth]{a5_t}%
      \includegraphics[width=0.166\textwidth]{a6_t}
    \end{subfigure}
	\newline
    \vspace{0.1ex}
    \begin{subfigure}{\textwidth}
      \includegraphics[width=0.166\textwidth]{a1_z}\llap{
      \parbox[b]{0.166\textwidth}{\caption{\vspace{-2ex}\label{fig:z} \textnormal{\textbf{\color{white}{jaw$+$tongue}}}}\rule{0ex}{-0.1\textwidth}}}%
      \includegraphics[width=0.166\textwidth]{a2_z}%
      \includegraphics[width=0.166\textwidth]{a3_z}%
      \includegraphics[width=0.166\textwidth]{a4_z}%
      \includegraphics[width=0.166\textwidth]{a5_z}%
      \includegraphics[width=0.166\textwidth]{a6_z}
    \end{subfigure}
    \newline
    \vspace{0.1ex}
    {
    \captionsetup[sub]{ % The subcaption settings
    labelfont=black}        % Use our custom label format
	\begin{subfigure}{\textwidth}%
		\includegraphics[width=\textwidth]{strategy.png}\llap{
      \parbox[b]{\textwidth}{\caption{\vspace{-2ex}\textnormal{\textbf{\label{fig:strategy}}}}\rule{0ex}{-0.1\textwidth}}}%
	\end{subfigure}
    }
	\caption{Information extraction from real-time magnetic resonance imaging of the vocal tract during speech. 
    \subref{fig:transition}, sequence of MRI video frames of a transition from vowel [a] to vowel [i] in the sequence [aia] (for presentation purposes, frame rate was downsampled by factor of 2). 
    \subref{fig:contours}, contour tracking of the speech articulators. 
    \subref{fig:cd}, automatic measurement of constriction degree between tongue and hard palate (magenta).
    \subref{fig:j}, jaw contribution to change in constriction degree.
    \subref{fig:t}, tongue contribution to change in constriction degree.
    \subref{fig:z}, total change in constriction degree (sum of jaw and tongue contributions).
    \subref{fig:strategy}, jaw and tongue contributions to palatal constriction.}
	\label{fig:rtMRI}%
\end{figure*}


\begin{figure*}[ht]
\centering
\includegraphics[width=0.33\linewidth]{j-crop.eps}
\includegraphics[width=0.33\linewidth]{lt-crop.eps}
\includegraphics[width=0.33\linewidth]{z-crop.eps}
\caption{\label{fig:sds} Comparison of jaw biomarkers (left), lip/tongue biomarkers (center), and total constriction degree biomarkers between Scan~1 ($X$-axis) and Scan~2 ($Y$-axis). Solid lines indicate standard deviations. Dashed line indicates equality between Scans~1 and~2.}
\end{figure*}

%An articulatory strategy characterizes the way in which the articulators move in order to perform a given speech task. 
%This section introduces a method for quantifying the articulatory strategies for forming and releasing vocal tract constrictions. 

\subsection{Image acquisition and reconstruction}
\label{subsec:imageacquisitionandreconstruction}

This study imaged healthy volunteers ($n=8$, 4 males, 4 females) in two scans on the same day using an imaging sequence which was specifically designed to capture the deformation of the airway at fast frame rate. Participants produced the sequences [apa], [ata], [aka], and [aia] 10 times per scan. A real-time spiral sequence based on the RTHawk platform (HeartVista, Menlo Park, CA, USA) with bit-reversed readout ordering was used. Sequence parameters were: field-of-view~\SI{200x200}{\milli\meter}, reconstructed resolution~\SI{2.4x2.4}{\milli\meter}, slice thickness~\SI{6}{mm}, TR~\SI{6}{\milli\second}, TE~\SI{3.6}{\milli\second}, flip angle~\SI{15}{\degree}, and \num{13} spiral interleaves for full sampling. 
The scan plane was manually aligned with the midsagittal plane of the subject's head. Images were retrospectively reconstructed to a temporal resolution of~\SI{12}{\milli\second} (\num{2} spirals per frame, \num{83} frames per second), resulting in an acceleration factor of \num{6.5}. Reconstruction was performed using the Berkeley Advanced Reconstruction Toolbox (BART)~\cite{uecker2015berkeley}.
The MRI sequence and experiment protocol was previously reported~(\cite{toger2017test},~\S2). 
Figure~\ref{fig:transition} shows a sequence of \num{6} rtMRI images of the release of a pharyngeal constriction for [a] and the subsequent formation of a palatal constriction for [i] in the sequence [aia]. 

\subsection{Time-point annotation}
\label{subsec:timepointannotation}

Vocal tract constrictions were manually identified in the rtMRI videos. The video frames were inspected on a computer monitor, and the intervals of time during which the vocal tract produced constrictions were manually identified by annotating the frame number of the first and last frames in which there was visible movement. 

\subsection{Contour tracking}
\label{subsec:contourtracking}

The contours of articulators were identified in the rtMRI videos and tracked automatically during vocal tract constrictions~\cite{bresch2009region}. The algorithm was manually initialized with templates matching the vocal tract contours during the sounds \textipa{[a], [i], [p], [t], [k]}. % If visual inspection revealed clear errors, then the algorithm initialization was manually corrected and the contours were re-submitted to the algorithm. This was repeated as needed until no clear contour tracking errors remained. 
Figure~\ref{fig:contours} shows a sequence of \num{6} contours which were tracked from the release of a pharyngeal constriction for [a] to the subsequent formation of a palatal constriction for [i] in the sequence [aia].

\subsection{Constriction degree measurement}
\label{subsec:constrictiondegreemeasurement}

Constrictions were quantified by measuring change in \textit{constriction degree} as a local descriptor of airway shape at a phonetic place of articulation. The constriction degree was the distance between the opposing structures at the place of articulation. The opposing structures were the upper and lower lips for [p], tongue and coronal place for [t], tongue and palatal place for [i], tongue and velar place for [k], tongue and pharyngeal place for [a]. 
%The upper lip, lower lip, and tongue were identified automatically by the contour tracking algorithm~\cite{bresch2009region}. The remaining structures were manually specified in midsagittal MRI using a custom MATLAB graphical user interface. 
%The coronal place was bounded anteriorly by the gingival margin of the upper incisors and posteriorly by the alveolar ridge (i.e., the inflection point marking the superior-deflection of the hard palate). The palatal place was bounded anteriorly by the alveolar ridge and posteriorly by junction between the hard and soft palate. The velar place was bounded anteriorly by the junction between the hard and soft palate and posteriorly by the uvula. The pharyngeal place was bounded superiorly by the nasopharynx and inferiorly by the larynx. 
Five phonetic places of articulation were obtained, corresponding to the labial, coronal, palatal, velar, and pharyngeal places of articulation. 
The degree of constriction was measured automatically at the phonetic places of articulation in each video frame. Bilabial constriction degree was the minimum distance between the upper lip and lower lip. Constriction degrees in the oral cavity and pharynx were the minimum distances from the tongue to the coronal, palatal, velar, and pharyngeal place.
Figure~\ref{fig:cd} illustrates the measurement of constriction degree at the hard palate in \num{6} rtMRI images of the release of a pharyngeal constriction for [a] and the subsequent formation of a palatal constriction for [i] in the sequence [aia]. 

\subsection{Factor analysis of vocal tract shapes}
\label{subsec:factoranalysisofvocaltractshapes}

Articulator positions were expressed as the linear combination of factors which reflected the principal directions of spatial variability for the jaw, tongue, and lip contours~\cite{toutios2015factor}. Jaw, tongue, and lip factors were obtained for each participant separately. Varying the coefficients of the linear combination of factors expressed articulator movements. 

One jaw factor characterized the spatial variability of the jaw along with the jaw-associated variability of the tongue and lips. Let $n$ be the number of rtMRI video frames. Let $p$ be the number of articulator contour vertices in each frame. Let $Y_\text{j}$ be the $n\times p$ matrix of articulator contour vertices with the vertices of non-jaw contours set to zero. 
Define the covariance matrix as $R_\text{j}=Y_\text{j}^\intercal Y_\text{j} / (n-1)$. 
Let $Y_\text{jtl}$ be the $n\times p$ matrix of articulator contour vertices with the vertices of non-jaw, non-tongue, and non-lip contours set to zero.
The first principal component $v_1$ of $Y_\text{j}$ was normalized to have unit variance as $h_1 = v_1 / (v_1^\intercal R_\text{jtl} v_1)$ and obtained the jaw factor as $u_1 = R_\text{jtl} h_1$.
This factor captured jaw motion and the associated lip and tongue motion.

Four tongue factors characterized the spatial variability of the tongue, independently of the jaw. Let $Y_\text{t}$ be the $n\times p$ matrix of articulator contour vertices with the vertices of non-tongue contours set to zero. 
Variance which was due to the jaw factor was subtracted from the tongue contours to obtain $Y_\text{t}' = Y_\text{t} (I - u_1 u_1^* )$, where $*$ denotes the Moore-Penrose pseudoinverse~\cite{albert1972regression}. 
Define the covariance matrix as $R_\text{t}' =Y_\text{t}'^\intercal Y_\text{t}' / (n-1)$. 
The first principal component $v_2$ of $Y_\text{t}'$ was normalized to have unit variance as $h_2 = v_2 / (v_2^\intercal R_\text{t}' v_2)$ and obtained the first tongue factor as $u_2 = R_\text{t}' h_2$. 
The second tongue factor was obtained as $u_3 = R_\text{t}'' h_3$, where $R_\text{t}'' = Y_\text{t}''^\intercal Y_\text{t}'; / (n-1)$, $Y_\text{t}'' = Y_\text{t}'(I - h_2 h_2^*)$, and $h_3 = v_3 / (v_3^\intercal R_\text{t}' v_3)$. This pattern was iterated to obtain $h_4$ and $h_5$ for a total of four tongue factors. 

Two lip factors $h_6$, $h_7$ characterized the spatial variability of the lips, independently of the jaw. The lip factors were obtained in the same way as the tongue factors.

Row $y_n^\intercal $ of $Y$ contained the articulator contours in rtMRI video frame $n$. The column vector $y_n$ was parameterized as the linear combination $y_n = H w_n$, where $H$ was the $p\times 7$ matrix with columns $h_1, h_2, \ldots , h_7$ and $w_n$ was the vector of coefficients $w_{1n}, w_{2n}, \ldots , w_{7n}$ which parameterized the articulator contours in rtMRI video frame $n$.

\subsection{Definition of biomarkers}
\label{subsec:articulatorystrategies}

Change in a constriction degree $z_i$ during a vocal tract constriction was decomposed into the contributions of the jaw, lips, and tongue.  
The decomposition relied on the \textit{forward kinematic map}, a nonlinear function which maps articulator positions to the corresponding constriction degrees. We obtained the forward kinematic map using locally weighted regression~\cite{lammert2013statistical}. The jacobian $J$ of the forward kinematic map quantified the change $\Delta z$ in constriction degrees which was due to a small change $\mathrm{d}w$ in articulator positions. For constriction degree $z_i$, the jacobian of the forward kinematic map provided the following relation between constriction degrees and articulators:
\begin{align}
\begin{split}
\int_{0}^{T} \dot{z}_i \, \mathrm{d}t
	&= \int_{0}^{T} J_i\dot{w} \, \mathrm{d}t \\
    &= \sum_{k=1}^7 \int_{0}^{T} J_i P_k \dot{w} \, \mathrm{d}t
\end{split}
\end{align}
where $J_i$ is row $i$ of $J$, time $0$ is the temporal onset of a constriction, time $T$ is the temporal offset of a constriction, and the $7\times 7$ diagonal projection matrix $P_k$ ($kk$-entry equal to unity and all other entries equal to zero) broke the integral down into the contributions of each coefficient $w_k$.
Term $k$ of the outer summation is the theoretical contribution of factor $h_k$ to elapsed change in $z_i$ during a constriction. 

We defined $\lambda$ as the \textit{quantitative imaging biomarker of articulatory strategy}. $\lambda$ reflected the contribution of an individual articulator to a constriction. 
\begin{align}
\begin{split}
\lambda 
	&= \sum_{k\in \mathcal{U}} \int_{0}^{T} J_i P_k \dot{w} \, \mathrm{d}t\\
	&\sim \sum_{k\in \mathcal{U}} \sum_{n=0}^{N} J_i P_k \left(\frac{w_{n+1} - w_{n-1}}{2h}\right)
\end{split}
\end{align}
The set $\mathcal{U}$ depended on the articulator: $\mathcal{U}=\{1\}$ for the jaw; $\mathcal{U}=\{2,3,4,5\}$ for the tongue; and $\mathcal{U}=\{6,7\}$ for the lips.
Figure~\ref{fig:rtMRI} graphs the individual contributions of the jaw (\ref{fig:j}) and tongue (\ref{fig:t}) to the formation of the palatal constriction imaged in Figure~\ref{fig:transition}. Figure~\ref{fig:z} graphs the whole constriction as the sum of the jaw and tongue contributions.

\subsection{Test-retest repeatability}
\label{subsec:testretest}

A test-retest repeatability framework was adopted in order to determine how much the contributions of the jaw, lips, and tongue to vocal tract constrictions varied depending on how much the quantitative imaging biomarker of articulatory strategy depended on participant positioning within the scanner bore and short-term physiological variability. 

Agreement between Scan~1 and Scan~2 was quantified using the intraclass correlation coefficient (ICC). The ICC is a quantitative measure of test-retest repeatability for biomarkers. On the basis of a recent review~\cite{lebreton2007answers}, ICC values were categorized as poor (\num{0.00} to \num{0.30}), weak (\num{0.31} to \num{0.50}), moderate (\num{0.51} to \num{0.70}), strong (\num{0.71} to \num{0.90}), and very strong (\num{0.91} to \num{1.00}).

The ICC was computed using a linear mixed effects model fitted with the package lme4~\cite{bates2015fitting} in R~\cite{r2017language}. Consider the sample of $n=8$ participants, each with $k=20$ repeated measurements of articulatory strategy (\num{10} from Scan~1, \num{10} from Scan~2). The contribution $\lambda _{ij}$ for replicate measurement $j$ and participant $i$ was 
$\lambda _{ij} = \mu + p_i + e_{ij}$,
where $\mu$ is the group mean, $p_i$ is the random intercept for participant $i$, and $e_{ij}$ is the error. The random effects $p_i$ and $e_{ij}$ are independently and identically distributed with mean 0 and the inter-speaker variance $\sigma_p^2$ and intra-speaker variance $\sigma_e^2$ to be estimated from the data using restricted maximum likelihood. 

One ICC value was obtained for each articulator (i.e., jaw, lips, tongue) in each constriction type (i.e., bilabial closure, bilabial release, coronal closure, coronal release, palatal approximation, velar closure, velar release, pharyngeal approximation) as $\text{ICC}(\lambda) = \hat{\sigma}_p^2\big/ (\hat{\sigma}_p^2 + \hat{\sigma}_e^2)$.

% FIGURE 1


\section{Results}
 
Table~\ref{tab:icc} shows the intra-class correlation coefficients for the jaw, lips, and tongue contributions to vocal tract constrictions. Reproducibility of jaw contributions to vocal tract constriction ranged from poor to strong, with ICC ranging from \num{0.13} (velar closure) to \num{0.81} (bilabial closure and release). 
Reproducibility of lip/tongue contribution to vocal tract constriction ranged from weak to strong, with ICC ranging from \num{0.35} (pharyngeal approximation) to \num{0.79} (bilabial closure). 
Reproducibility of total change in vocal tract constriction degree ranged from poor to strong, with ICC ranging from \num{0.27} (pharyngeal approximation) to \num{0.76} (bilabial closure). 

\begin{table}[t]
	\centering
	\begin{tabular}{l l l l l}
    			& 					& jaw	& lips/tongue & total \B \\
	\cline{3-5}
	\multirow{ 2}{*}{bilabial} 
    			& closure			& 0.81		& 0.79 & 0.76 \T \\
        		& release			& 0.81 		& 0.65 & 0.64 \B \\
    \hline
    \multirow{ 2}{*}{coronal} 	
    			& closure			& 0.35 		& 0.57 & 0.59 \T \\
    			& release			& 0.66 		& 0.5  & 0.71 \B \\
	\hline
    palatal
    			& approximation		& 0.22		& 0.57 & 0.67 \T \B \\
	\hline
    \multirow{ 2}{*}{velar}		
    			& closure			& 0.13		& 0.55 & 0.60 \T \\
    			& release			& 0.26 		& 0.54 & 0.51 \B \\
    \hline
    pharyngeal
    			& approximation		& 0.35		& 0.35 & 0.27 \T \B \\
	\hline
	\end{tabular}
    \caption{Intra-class correlation coefficients for the jaw contributions, lips/tongue contributions, and total constriction.}
    \label{tab:icc}
    \vspace{-3em}
\end{table}

Table~\ref{tab:sd} shows the mean intra-speaker standard deviations for the jaw, lips, and tongue contributions to vocal tract constrictions. The mean intra-speaker standard deviation for jaw contribution to vocal tract constriction ranged from~\SI{1.25}{\milli\meter} (palatal approximation) to~\SI{0.37}{\milli\meter} (velar release).
The mean intra-speaker standard deviation for lips/tongue contribution to vocal tract constriction ranged from~\SI{1.89}{\milli\meter} (bilabial release) to~\SI{1.20}{\milli\meter} (coronal release).
The mean intra-speaker standard deviation for total change in vocal tract constriction degree ranged from~\SI{2.38}{\milli\meter} (bilabial release) to~\SI{1.15}{\milli\meter} (coronal release). See Figure~\ref{fig:sds} for scattergrams of the biomarkers.

\begin{table}[t]
	\centering
	\begin{tabular}{l l l l l}
    			& 					& jaw	& lips/tongue & total \B \\
	\cline{3-5}
	\multirow{ 2}{*}{bilabial} 
    			& closure			& 1.22		& 1.63 & 2.20 \T \\
        		& release			& 1.00 		& 1.89 & 2.38 \B \\
    \hline
    \multirow{ 2}{*}{coronal} 	
    			& closure			& 1.19 		& 1.81 & 1.53 \T \\
    			& release			& 0.76 		& 1.20 & 1.15 \B \\
	\hline
    palatal
    			& approximation		& 1.25		& 1.63 & 1.22 \T \B \\
	\hline
    \multirow{ 2}{*}{velar}		
    			& closure			& 0.46		& 1.66 & 1.53 \T \\
    			& release			& 0.37 		& 1.63 & 1.52 \B \\
    \hline
    pharyngeal
    			& approximation		& 0.43		& 1.57 & 1.57 \T \B \\
	\hline
	\end{tabular}
    \caption{Intra-speaker standard deviations (\si{\milli\meter}) for the jaw contributions, lips/tongue contributions, and total constriction.}
    \label{tab:sd}
    \vspace{-1.5em}
\end{table}

\section{Discussion}

This study introduced and evaluated a computational method for extracting quantitative imaging biomarkers of articulatory strategy. Articulatory strategies indicated how much each participant used the jaw, lips, and tongue to make vocal tract constrictions. Precision was high for most tongue biomarkers and for jaw biomarkers of anterior vocal tract constrictions. Precision was low for biomarkers of small-amplitude jaw movements and pharyngeal constrictions. 
Mean intra-speaker standard deviations were smaller than the \SI{2.4}{\milli\meter} pixel size of the rtMRI videos, indicating that the articulatory strategy biomarker had spatial resolution comparable to that of the rtMRI data from which it was extracted.

Building on existing rtMRI methods of parametric estimation and error analysis for Task Dynamics models~\cite{Sorensen+2016}, we plan to exploit the theoretical basis of the proposed computational method in the Task Dynamics framework in order to estimate parameters of articulatory strategy (i.e., articulator weights~\cite{saltzman1989dynamical}). The distribution of articulatory strategy biomarkers offers a basis on which to introduce stochasticity into Task Dynamics. This paper is the first to introduce and evaluate quantitative imaging biomarkers of articulatory strategies. \vspace{0.2cm} \\
\textbf{Acknowledgments:} Work supported by NIH grant R01DC007124 and NSF grant 1514544.

\bibliographystyle{IEEEtran}

\bibliography{mybib}

\end{document}