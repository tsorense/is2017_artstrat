
% Get directories
dirs = dir(fullfile(cd,'wrappers*'));
dirflag = [dirs.isdir];
dirs = dirs(dirflag);

% Make cluster files
for i=1:length(dirs)
    disp(dirs(i).name)
    cd(dirs(i).name)
    system('scp -r cluster/ hpc:/home/rcf-proj2/tjs/tanner');
    cd ..
end