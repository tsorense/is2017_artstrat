% VIEW_SEGMENTATION - view the segmentation results by making videos 
% overlaying the tissue-air boundaries on the video frames from the avi 
% files.
% 
% Note that NSF and Variability datasets require track files to be renamed
% using the files renamer_nsf.sh and renamer_var.sh before being viewed
% using this script.
% 
% Tanner Sorensen
% Signal Analysis and Interpretation Laboratory
% Feb. 14, 2017

dataset = 'rep2'; % change as needed

addpath ../util
configStruct = config;

addpath(genpath('a_functions'))
aviPath = configStruct.aviPath;
trackPath = configStruct.trackPath;

tab = readtable(configStruct.(sprintf('timestamps_file_name_%s',dataset)),'TreatAsEmpty','NA');
fl = tab.file;
folders = tab.speaker;
item = 'ptki';
count = 0;

for i=1:length(folders) % find(cellfun(@(x) strcmp(x,'pp1_nsf'),folders))'
    if ismember(folders{i},configStruct.(sprintf('folders_%s',dataset)))
        disp(folders{i})
        avifile = fullfile(aviPath,folders{i},'avi',fl{i});
        nRep = sum(~isnan(table2array(tab(cellfun(@(x) strcmp(x,folders{i}),folders),3))));
        for j=1:length(item)
            colIdx = cellfun(@(x) ~isempty(strfind(x,sprintf('A%sa',item(j)))),tab.Properties.VariableNames);
            if any(~isnan(table2array(tab(i,colIdx))))
                disp(item(j))
                track_fl = dir(fullfile(trackPath,sprintf('%s_*.mat',folders{i})));
                track_fl = {track_fl.name};
                track_file_idx = cellfun(@(x) ~isempty(strfind(x,[folders{i} '_a' item(j) 'a_' num2str(count)])), track_fl);
                track_file = track_fl{track_file_idx};
                contourfile = fullfile(trackPath,track_file);
                outfile = strrep(contourfile,'.mat','.avi');
                make_track_video_bounded(avifile, contourfile, outfile)
            end
        end
        count = count+1;
        if mod(i,nRep)==0, count = 0; end
    end
end
