function make_hpc_csv_rep(subj_id,configStruct,path_in,path_out)
% This script makes the csv file for the articulator segmentation, which
% runs on the HPC cluster. It saves the CSV file.
% 
% Input: 
%  SUBJ_ID - string subject id
%  CONFIGSTRUCT - 
%  PATH_IN - string path where CSV file can be read in
%  PATH_OUT - string path where CSV file should be saved
% 
% Output: 
%  none
% 
% Usage:
%  >> make_hpc_csv('at1_rep', ...
%       '/home/tsorense/spring2017/data', ...
%       '/home/tsorense/spring2017/strategies/timestamps.xlsx', ...
%       '/home/tsorense/spring2017/segmentation/wrappers_at1_rep')
% 
% Tanner Sorensen
% Signal Analysis and Interpretation Laboratory
% Feb. 2, 2017

aviPath = configStruct.aviPath;
framespersec = configStruct.framespersec_rep;

table_in = readtable(path_in);

fileID = fopen(fullfile(path_out,'segments.csv'),'w');

counter = 0;
for i=1:size(table_in,1)
    if strcmp(table_in.speaker{i},subj_id)
        aviFile = fullfile(aviPath,subj_id,'avi',table_in.file{i});

        start = table_in.Apa_clo_start(i)*1000/framespersec;
        stop = table_in.Apa_rel_end(i)*1000/framespersec;
        job_id = sprintf('%s_apa_%d',subj_id,counter);
        fprintf(fileID,'%s,%s,%f,%f\n',aviFile,job_id,start,stop);

        start = table_in.Ata_clo_start(i)*1000/framespersec;
        stop = table_in.Ata_rel_end(i)*1000/framespersec;
        job_id = sprintf('%s_ata_%d',subj_id,counter);
        fprintf(fileID,'%s,%s,%f,%f\n',aviFile,job_id,start,stop);

        start = table_in.Aka_clo_start(i)*1000/framespersec;
        stop = table_in.Aka_rel_end(i)*1000/framespersec;
        job_id = sprintf('%s_aka_%d',subj_id,counter);
        fprintf(fileID,'%s,%s,%f,%f\n',aviFile,job_id,start,stop);

        start = table_in.Aia_pal_clo_start(i)*1000/framespersec;
        stop = table_in.Aia_phar_clo_end(i)*1000/framespersec;
        job_id = sprintf('%s_aia_%d',subj_id,counter);
        fprintf(fileID,'%s,%s,%f,%f\n',aviFile,job_id,start,stop);
        
        counter = counter+1;
    end
end

end