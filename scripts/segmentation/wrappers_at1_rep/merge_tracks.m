% I HAVE THIS IN THE FOLDER WHERE I STORE MY TRACKED .MAT FILES

trackdatafull=[];

segmentFile = '../csv/at1_rep.csv';
firstLine = 2;
lastLine = 4;

fid = fopen(segmentFile);

lineCount=1;

while lineCount<firstLine
    
    fgetl(fid);
    lineCount = lineCount + 1;
    
end;

while lineCount<lastLine+1
    
    str = fgetl(fid);
    celldata = textscan(str,'%s','Delimiter',',');
    strdata = celldata{1};
    coreFilename = char(strdata(2));
    
    filename = [coreFilename,'_track.mat'];
    
    load(filename);
    
    trackdatafull=[trackdatafull,trackdata];
    
    lineCount = lineCount + 1;
    
end;

fclose(fid);