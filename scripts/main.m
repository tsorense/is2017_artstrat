% MAIN - extracts articulatory strategy biomarkers from the test-retest
% dataset
% 
% Tanner Sorensen
% Signal Analysis and Interpretation Laboratory
% Feb. 14, 2017

%% Step 0. Set parameters of the analysis and constants variables
addpath util
configStruct = config;

%% Step 2. Convert contour data format
cd data_scripts
wrap_make_contour_data
cd ..

%% Step 3. Guided factor analysis
cd factor_analysis
main
cd ..

%% Step 4. Measure constriction degrees
cd constrictions
main
cd ..

%% Step 5. Quantify articulatory strategies
cd strategies
main
cd ..
