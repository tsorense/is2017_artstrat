function table = fetch_gem_data(cat)
%
%   FUNCTION:
%   extract all data from singleton-geminate duration comparison structures
%
%   USAGE:
%   table = fetch_gem_data( 'lab'/'cor'/'dors' );
%
%

    dat = [];
    j	= 0;
    
    vars	= evalin('base','whos');
    ndat	= length(vars);
    for i = 1:ndat
        if strcmp(vars(i).class,'struct')
            nv	= vars(i).name;
            if strmatch( cat,nv )
                j	= j+1;
                dat(j).tok	= evalin('base', [nv '.tok1']);
                dat(j).dur	= evalin('base', [nv '.dur1']);
                dat(j).area	= evalin('base', [nv '.area1']);
                j	= j+1;
                dat(j).tok	= evalin('base', [nv '.tok2']);
                dat(j).dur	= evalin('base', [nv '.dur2']);
                dat(j).area	= evalin('base', [nv '.area2']);
            end
        end
    end
    
    table = dataset( {{dat.tok}','Token'},  {[dat.dur]','Duration'}, {[dat.area]','Area'} );

end %of main function
