function plot_frames( dat, frames )
%
%    plot_frames( avi, frames );
%

    avi	 = dat.vid.avi;
    cm	 = gray(255);
    lwd	 = 1;

    figure;
    fc	= frames( round(length(frames)/2) );
    image(avi(fc).cdata);
    colormap(cm);
    axis square; axis off;

    hold on;
    for fnum = frames
        if ~isempty( dat.vt(fnum).pts )
            glot  = dat.vid.grid.glot;
            inner = [dat.vt(fnum).pts.lf];
            outer = [dat.vt(fnum).pts.rt];
            ibnd  = [glot inner];
            obnd  = [glot outer ibnd(end-1:end)];
            line( ibnd(1:2:end),ibnd(2:2:end), 'Color','g', 'Linewidth',lwd );
            line( obnd(1:2:end),obnd(2:2:end), 'Color','g', 'Linewidth',lwd );
        end
    end


end %of main function
