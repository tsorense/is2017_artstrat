function plot_tract( dat, fnum )
%
%    plot_tract( avi, fnum );
%

    cm	 = gray(255);
    avi	 = dat.vid.avi;

    figure;
    image(avi(fnum).cdata);
    colormap(cm);
    axis square; axis off;

    if ~isempty( dat.vt(fnum).pts )
        glot  = dat.vid.grid.glot;
        inner = [dat.vt(fnum).pts.lf];
        outer = [dat.vt(fnum).pts.rt];
        ibnd  = [glot inner];
        obnd  = [glot outer ibnd(end-1:end)];
        line( ibnd(1:2:end),ibnd(2:2:end), 'Color','g','LineWidth',2 );
        line( obnd(1:2:end),obnd(2:2:end), 'Color','g','LineWidth',2 );
    end


end %of main function
