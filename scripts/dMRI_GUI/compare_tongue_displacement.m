function lab = compare_tongue_displacement( varargin )
%
%	compare_tongue_displacement( dat1,lab1, ... ,datn,labn, gl );
%
%   superimpose multiple plots of tongue postures for comparison
%   calculatea and plot mean tongue posture
%	tongue edges extracted from centres of labeled sequences
%
%   eg. compare_tongue_displacement( zf1b,'asa', zf1b,'asa','zf' );
%

    % fetch range of frames defined by each labeled sequence
    nlabels	= (nargin-1)/2;
    for i = 1:nlabels
        dat     = varargin{2*i-1};
        label	= char(varargin{2*i});
        fi      = dat.seg.(label).fint;
        lab(i).nf	= diff(fi) + 1;
        lab(i).ff	= fi(1):fi(2);
        lab(i).txt	= label;
    end
    gl	= varargin{nargin};
    
    % calculate change in tract aperture for frame seq assoc with ea label
    for i = 1:nlabels
        dat	= varargin{2*i-1};
        ixg = 0;
        lab(i).gap = zeros(1,lab(i).nf);
        for g = gl
            ixg = ixg+1;
            ixf = 0;
            for f = lab(i).ff
                ixf = ixf+1;
                if ~isempty( dat.vt(f).pts )
                    pts	= dat.vt(f).pts(g);
                    lab(i).gap(ixg,ixf) = norm( pts.lf-pts.rt );
                end
            end
        end
    end
    
    % create gradient colormap same size as vector to be plotted
    colspec	= 'autumn';     % 'winter';
    ncol	= length(gl)+0;
    cmap	= eval(['flipud( ' colspec '(' num2str(ncol) '))']);
    
    % plot time function of traqct aperture
    figure; hold on;
    lwd	= 2;
    for i = 1:nlabels
        cix	= 1;
        for g = 1:ixg
            plot( lab(i).gap(g,:), 'Color',cmap(cix,:), 'Linewidth',lwd );
            cix = cix+1;
        end
    end

end %of main function
