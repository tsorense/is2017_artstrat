
======================================
BRIEF GUIDE TO THE <inspect_dmri> TOOL
                             13-mar-14
======================================


INTRODUCTION
------------
This document provides a brief overview of the tool <inspect_dmri>, a Matlab GUI designed to provide an integrated interface for inspection and analysis of companion audio and video files recorded during dynamic MRI sessions.


FILES
-----
<inspect_dmri.m>		main program mfile: audio-visual GUI interface
<calc_sg.m>			support mfile: compute spectrogram
<calc_subplot_dims.m>		support mfile: compute figure subplot geometry
<dijkstra.m>			support mfile: dijkstra shortest path implementation (D.F. Gleich 2009)
<extrema.m>			support mfile: find local minima & maxima (C.A.V. Aguilera 2004)
<fetch_fmts.m>			support mfile: find formant trajectories
<freezeColors.m>		support mfile: allow different colormaps on figure subplots (J. Iversen 2005)
<mri_intensitycorrect.m>	support mfile: allow different colormaps on figure subplots (A.Lammert 2005)


DIRECTORY STRUCTURE
-------------------
The GUI expects to operate in a directory which contains two subdirectories:
	<./wav>				directory containing dMRI audio files (*.wav)
	<./avi>				directory containing dMRI video files (*.avi: without sound)
... and an optional third subdirectory:
	<./avi_trak>			directory containing dMRI tracked (E. Bresch 2007) video files (*.avi)

Corresponding files in each directory must use the same base filename. For example, a complete set of data from an MRI session might be represented by the following three companion files, stored in the following locations. 			
	<dmri/wav/kaLi.wav>
	<dmri/avi/kaLi.avi>
	<dmri/avi_trak/kaLi.avi>

With this file structure in place, the GUI could then by run from the directoy <dmri>. The GUI will function if the tracked AVI is missing, however at least one video file and a matching audio file must be available.


INVOCATION
----------
From the Matlab command window, start the GUI with the function call:

	dat = inspect_dmri( 'corpus' );

... where 'corpus' corresponds to the base filename of the audio and video files to be analysed. There will be a short delay as the GUI load and searches for the audio and video files. Missing files will be reported, and the GUI will die gracefully if a minimum set of data files cannot found. If the untracked AVI file is found, there will be a longer delay while the video is pre-scaled to the same dimensions as the tracked AVI video - a waitbar will monitor progress.


DATA PRESENTATION AND REVIEW
----------------------------
Once the dMRI data has been loaded, synchronous sections of video and audio can be reviewed together. The GUI is divided into three panes: video in the left pane, audio in the centre pane, and additional controls on the right.

There are two audio displays: the top axes present the entire audio signal; the lower set of axes present a zoomed-in view of a smaller interval of the signal. The zoomed display defaults to a 1 second window, but this can be adjusted using the controls to the right.

At all times, the currently-displayed video frame corresponds to the point in time indicated by the dashed vertical line on the audio displays. By default, the GUI initially displays the video frame which occurs 2 seconds after the start of the AVI file. Another point in time can be selected by: 
 - clicking on either of the audio displays
 - adjusting the slider beneath the video display
 - entering a specific frame number into the text box beneath the video display
 - entering a specific time (in seconds) into the text box beneath the video display

There are two video display modes available: 'Plain AVI' and 'Tracked AVI'. If only one type of AVI file was loaded, the GUI will default to displaying video data in the appropriate format. If both original and tracked AVI files were loaded successfully, the user can switch between the two video modes using the radio button in the video control pane.

At any point, the section of the MRI data under inspection can be audited. Using the controls at the right of the GUI, the user can audition the currently-selected interval of audio, the currently-selected section of video, or the entire file. The section of signal which will be replayed is delineated by the interval selected in the lower audio signal inspection window: by default, 1 second of audio or video will be played; if the width of the zoom window is decreased, a smaller interval will be replayed.


FRAME CAPTURE
-------------
Once the user has navigated to a point of interest in the signal, the video frame at that point can be captured. A series of buttons is arranged at the bottom of the video review pane. By clicking on any of these, the currently displayed video frame will be saved as a JPG file, automatically labeled with the selected articulator, corpus name and frame number. eg:
	<tb_kaLi_f44_s.jpg>

Both tracked and untracked frames are written to image files, in a subdirectory created in the base directy from where the GUI was invoked.
 

KNOWN BUGS
----------
  All OS:	export of intensity-corrected images frames breaks with some colormaps
Mac OS-X:	video playback inverted on some codecs


AUTHORS
-------
Michael Proctor		mike.i.proctor@gmail.com
Adam Lammert		alammert@gmail.com
Asterios Toutios	toutios@gmail.com


CHANGE LOG
----------
16-jan-2010:		first public release
22-jan-2010:		pixel intensity correction routines added
13-mar-2014:		aviread functionality replaced by VideoReader to comply with latest Matlab releases 

