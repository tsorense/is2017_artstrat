function [AF, AFm,AFa,dAF] = calc_af_err(dat,frames)
%
% [AF, AFm,AFa,dAF] = calc_af_err( dat, frames )
%

    minlenAF = Inf;
    nf = length(frames);
    for fix = 1:nf
        f = frames(fix);
        AF(fix).AFa	= dat.vt(f).AFa;
        AF(fix).AFm	= dat.vt(f).AFm;
        AF(fix).AFx	= dat.vt(f).AFx;
        AF(fix).dAF	= abs(AF(fix).AFm-AF(fix).AFa);
        if (length(AF(fix).AFx) < minlenAF)
            minlenAF = length(AF(fix).AFx);
        end
    end
    
    AFm = zeros(nf,minlenAF);
    AFa = zeros(nf,minlenAF);
    AFx = zeros(nf,minlenAF);
    dAF = zeros(nf,minlenAF);
    for fix = 1:nf
        AFm(fix,:)	= AF(fix).AFm(1:minlenAF);
        AFa(fix,:)	= AF(fix).AFa(1:minlenAF);
        AFx(fix,:)	= AF(fix).AFx(1:minlenAF);
        dAF(fix,:)	= AF(fix).dAF(1:minlenAF);
    end
    
    mAFm	= mean(AFm);
    mAFa	= mean(AFa);
    mdAF	= mean(dAF);
    mAFx	= mean(AFx);

    hAF = figure;
    set( hAF, 'Name',['Vocal Tract Area Functions: Subject BS, ' num2str(nf) ' frames'] );
    %set( hAF, 'ToolBar','none', 'MenuBar','none' );
    set( hAF, 'ToolBar','none' );
    stairs( mAFx,mAFm, 'r--', 'LineWidth',2 ); hold on
    stairs( mAFx,mAFa, 'b-',  'LineWidth',2 );
    stairs( mAFx,mdAF, 'k:',  'LineWidth',2 );
    %axis equal;
    axis tight;
    %yy = ylim; ylim([0 yy(2)]);
    xlabel('Distance from Glottis (mm)');
    ylabel('Aperture (mm)');

    
end
