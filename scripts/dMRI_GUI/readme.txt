
DESCRIPTION
-----------
INSPECT_dMRI is a Matlab GUI designed to provide an integrated interface for inspection and analysis of companion audio and video files recorded during dynamic MRI sessions.


REQUIREMENTS
------------
- Matlab v.2007a or later, running on Linux, XP, Vista, or Windows 7
- Matlab running on OS-X will also work, however GUI renders poorly on Mac versions Matlab


QUICK START
-----------
- unzip archive <inspect_dmri.zip>
- start Matlab
- update Matlab path to include directory containing file <inspect_dmri.m>
- start GUI by issuing command "inspect_dmri('kaLi')" in Matlab shell
