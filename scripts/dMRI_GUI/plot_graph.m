function plot_graph( dat, fnum )
%
%    plot_grid( dat, fnum );
%

    dcr	 = 5;               % size of cross at displayed points
    cm	 = gray(255);
    avi	 = dat.vid.avi;
    grid = dat.vid.grid;
    lw	 = 1;

    figure; image(avi(fnum).cdata);
    colormap(cm);
    axis square;

    cx = grid.glot(1);      % place cross at glottis
    cy = grid.glot(2);
    line( [cx-dcr cx+dcr-1], [cy cy], [0 0], 'Color','b', 'Linewidth',lw );
    line( [cx cx], [cy-dcr cy+dcr-1], [0 0], 'Color','b', 'Linewidth',lw );
    
    cx = grid.or1(1);       % place cross at lingual origin
    cy = grid.or1(2);
    line( [cx-dcr cx+dcr-1], [cy cy], [0 0], 'Color','b', 'Linewidth',lw );
    line( [cx cx], [cy-dcr cy+dcr-1], [0 0], 'Color','b', 'Linewidth',lw );
    
    cx = grid.or2(1);       % place cross at alveolar origin
    cy = grid.or2(2);
    line( [cx-dcr cx+dcr-1], [cy cy], [0 0], 'Color','b', 'Linewidth',lw );
    line( [cx cx], [cy-dcr cy+dcr-1], [0 0], 'Color','b', 'Linewidth',lw );
    
    for gl = 1:grid.nlines	% draw line across tract at each grid interval
        grd = grid.ends(gl);
        txt = grid.txt(gl);
        hGL = line( grd.x, grd.y, [0 0], 'Color','b', 'Linewidth',lw );                
        set( hGL, 'UserData',gl, 'HitTest','on', 'ButtonDownFcn',@gridline_sel );
        if ~mod(gl,5)
            text( txt.x,txt.y, num2str(gl), 'Color','b', 'Linewidth',lw );
        end
    end %for

    nodes = dat.vt(dat.fnum).Mxy;
    edges = dat.vt(dat.fnum).Mwt;
    for r = 1:rows(edges)
        for c = 1:cols(edges)
            if (edges(r,c))
                line( [nodes(r,1) nodes(c,1)],[nodes(r,2) nodes(c,2)], 'Color','k', 'MarkerSize',15 );
            end
        end
    end
    
    axis off;
    

end %of main function
