function dat = compare_gem_tokens( tok1,tok2, varargin )
%
%   FUNCTION:
%   compare duration and area under intensity peaks extracted from two tokens;
%   optionally plot intensity peaks
%
%   USAGE:
%   compare_gem_tokens( tok1,tok2, (pl) );
%
%   INPUTS:
%   tok1 (char):    segment label of 1st token to compare
%   tok2 (char):    segment label of 2nd token to compare
%   pl (int):       plot verbosity: 0: dont plot anything
%                                   1: plot intensity peaks
%
%   EXAMPLE:
%   dat = compare_gem_tokens( 'baco','bacco',1 );
%

    if (nargin < 3)
        pl = 0;
    else
        pl = varargin{1};
    end;

    set1        = find_token( tok1,0 );    set1 = char(set1(1));
    set2        = find_token( tok2,0 );    set2 = char(set2(1));
    dat.tok1	= tok1;
    dat.tok2	= tok2;
    dat.dur1	= evalin( 'base', [ set1 '.seg.' tok1 '.duration' ]);
    dat.dur2	= evalin( 'base', [ set2 '.seg.' tok2 '.duration' ]);
    dat.ddur    = dat.dur2 - dat.dur1;
    dat.area1	= evalin( 'base', [ set1 '.seg.' tok1 '.peak_area' ]);
    dat.area2	= evalin( 'base', [ set2 '.seg.' tok2 '.peak_area' ]);
    dat.dArea	= dat.area2 - dat.area1;
    
    if (pl)
        figure; hold on;
        yy1	= evalin( 'base', [ set1 '.seg.' tok1 '.Icurve' ]);
        yy2	= evalin( 'base', [ set2 '.seg.' tok2 '.Icurve' ]);
        plot( yy1, 'r' );
        plot( yy2, 'b' );
        legend( dat.tok1, dat.tok2 );
    end;


end %of main function
