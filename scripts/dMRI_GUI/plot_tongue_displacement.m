function gap = plot_tongue_displacement( dat,label,gl )
%
%    plot_tongue_displacement( dat,label,gl );
%

    % fetch range of frames defined by sequence label
    fi	= dat.seg.(label).fint;
    ff	= fi(1):fi(2);
    nf	= diff(fi) + 1;
    
    % calculate change in tract aperture across sequence of frames
    ixg = 0;
    gap = zeros(1,nf);
    for g = gl
        ixg = ixg+1;
        ixf = 0;
        for f = ff
            ixf = ixf+1;
            if ~isempty( dat.vt(f).pts )
                pts	= dat.vt(f).pts(g);
                gap(ixg,ixf) = norm( pts.lf-pts.rt );
            end
        end
    end

    % create gradient colormap same size as vector to be plotted
    colspec	= 'autumn';     % 'winter';
    cix     = 1;
    ncol	= length(gl)+0;
    cmap	= eval(['flipud( ' colspec '(' num2str(ncol) '))']);
    
    % plot lingual movement over specified time interval
    figure; hold on;
    lwd	= 2;
    for g = 1:ixg
        plot( gap(g,:), 'Color',cmap(cix,:), 'Linewidth',lwd );
        cix = cix+1;
    end

end %of main function

