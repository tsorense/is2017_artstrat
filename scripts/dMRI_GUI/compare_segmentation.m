function compare_segmentation( dat, fnum, mode )
%
%    compare_segmentation( dat, fnum, mode );
%
%    mode = 'man'/'pts'
%

    dcr	 = 5;               % size of cross at displayed points
    cm	 = gray(255);
    avi	 = dat.vid.avi;
    grid = dat.vid.grid;
    lwd	 = 1;

    figure;
    image(avi(fnum).cdata);
    colormap(cm);
    axis square; axis off;

    cx = grid.glot(1);      % place cross at glottis
    cy = grid.glot(2);
    line( [cx-dcr cx+dcr-1], [cy cy], [0 0], 'Color','b', 'Linewidth',lwd );
    line( [cx cx], [cy-dcr cy+dcr-1], [0 0], 'Color','b', 'Linewidth',lwd );
    
    cx = grid.or1(1);       % place cross at lingual origin
    cy = grid.or1(2);
    line( [cx-dcr cx+dcr-1], [cy cy], [0 0], 'Color','b', 'Linewidth',lwd );
    line( [cx cx], [cy-dcr cy+dcr-1], [0 0], 'Color','b', 'Linewidth',lwd );
    
    cx = grid.or2(1);       % place cross at alveolar origin
    cy = grid.or2(2);
    line( [cx-dcr cx+dcr-1], [cy cy], [0 0], 'Color','b', 'Linewidth',lwd );
    line( [cx cx], [cy-dcr cy+dcr-1], [0 0], 'Color','b', 'Linewidth',lwd );
    
    for gl = 1:grid.nlines	% draw line across tract at each grid interval
        grd = grid.ends(gl);
        txt = grid.txt(gl);
        line( grd.x, grd.y, [0 0], 'Color','b', 'Linewidth',lwd );                
        if ~mod(gl,5)
            text( txt.x,txt.y, num2str(gl), 'Color','b', 'Linewidth',lwd );
        end
    end %for

    if strcmp( mode, 'pts' )
        if ~isempty( dat.vt(fnum).pts )
            glot  = dat.vid.grid.glot;
            inner = [dat.vt(fnum).pts.lf];
            outer = [dat.vt(fnum).pts.rt];
            ibnd  = [glot inner];
            obnd  = [glot outer ibnd(end-1:end)];
            line( ibnd(1:2:end),ibnd(2:2:end), 'Color','g','LineWidth',2 );
            line( obnd(1:2:end),obnd(2:2:end), 'Color','g','LineWidth',2 );
        end
    end

    if strcmp( mode, 'man' )
        if ~isempty( dat.vt(fnum).man )
            glot  = dat.vid.grid.glot;
            inner = [dat.vt(fnum).man.lf];
            outer = [dat.vt(fnum).man.rt];
            ibnd  = [glot inner];
            obnd  = [glot outer ibnd(end-1:end)];
            line( ibnd(1:2:end),ibnd(2:2:end), 'Color','g','LineWidth',2 );
            line( obnd(1:2:end),obnd(2:2:end), 'Color','g','LineWidth',2 );
        end
    end


end %of main function
