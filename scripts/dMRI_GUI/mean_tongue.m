function mean_tongue( varargin )
%
%	mean_tongue( dat1,lab1, ... ,datn,labn, txt );
%
%   superimpose multiple plots of tongue postures for comparison
%   calculatea and plot mean tongue posture
%	tongue edges extracted from centres of labeled sequences
%
%   eg. mean_tongue( zf1b,'asa', zf1b,'asa','zf' );
%

    palcol = [0.6 0.6 0.6];
    
    % fetch center frame from each labeled sequence
    nlabels	= (nargin-1)/2;
    for i = 1:nlabels
        dat     = varargin{2*i-1};
        label	= char(varargin{2*i});
        fint	= dat.seg.(label).fint;
        lab{i}  = label;
        fc(i)	= round(mean(fint));
    end
    lab{i+1}  = 'Mean';
    
    % plot tract outline: centre frame of sequence
    hPLT = figure;	hold on;
    lwd	= 1;
    dat	= varargin{1};
    if ~isempty( dat.vt(fc(1)).pts )
        glot  = dat.vid.grid.glot;
        inner = [dat.vt(fc(1)).pts.lf];
        outer = [dat.vt(fc(1)).pts.rt];
        ibnd  = [glot inner];
        obnd  = [glot outer ibnd(end-1:end)];
        line( ibnd(1:2:end),ibnd(2:2:end), 'Color',palcol, 'Linewidth',lwd );
        line( obnd(1:2:end),obnd(2:2:end), 'Color',palcol, 'Linewidth',lwd );
    end
    
    % find range of lingual gridlines
    tgl = dat.vid.grid.tng;
    col	= flipud(autumn(nlabels+3));
    col	= col(3:end,:);
    
    % plot lingual configuration at center frame of each labeled sequence
    lwd	= 1;
    for i = 1:nlabels
        dat	= varargin{2*i-1};
        if ~isempty( dat.vt(fc(i)).pts )
            tongue(i,:)	= [dat.vt(fc(i)).pts(tgl).lf];
            h(i) = line( tongue(i,1:2:end),tongue(i,2:2:end), 'Color',col(i,:), 'Linewidth',lwd, 'Linestyle','--' );
        end
    end
    
    % plot mean mid-consonantal lingual configuration
    lwd	= 2;
    tongue(i+1,:) = mean(tongue);
    h(i+1)        = line( tongue(i+1,1:2:end),tongue(i+1,2:2:end), 'Color',col(i+1,:), 'Linewidth',lwd );
    
    % annotate and tidy up plots
    title( varargin{nargin} );
    axis square; axis tight; axis off;
    set( gca, 'YDir','reverse');
    set( gcf, 'color','white' );
    legend( h,lab, 'Location','SouthWest' );

end %of main function
