function segs = list_labels(dat)
%
%   FUNCTION:
%   list all segment labels in specified data structure, or
%   all segment labels in all data structures in current workspace
%
%   USAGE:
%   segs = list_labels();
%   segs = list_labels(dat);
%
%   INPUTS:
%   dat (string/struct):    optional input argument specifing name of data
%                           structure to search for labels
%
%

    segs = [];
    if (nargin)
        if ischar(dat)
            if evalin('base', ['isstruct(' dat ')'])
                if evalin('base', ['isfield( ' dat ',''seg'' )'])
                    toks = evalin('base', ['fieldnames(' dat '.seg)']);
                    segs = [segs; toks];
                end
            else
                fprintf('   Specified variable ''%s'' is not a structure.\n', dat );
            end
        else
            if isstruct(dat)
                if isfield( dat,'seg' )
                    toks = fieldnames( dat.seg);
                    segs = [segs; toks];
                end
            else
                fprintf('   Input variable ''%s'' is not a structure.\n', inputname(1) );
            end
        end
    else
        dat     = evalin('base','whos');
        ndat	= length(dat);
        for i = 1:ndat'
            if strcmp(dat(i).class,'struct')
                nv	= dat(i).name;
                if evalin('base', ['isfield(' nv ',''seg'')'])
                    toks = evalin('base', ['fieldnames(' dat(i).name '.seg)']);
                    segs = [segs; toks];
                end
            end
        end
    end


end %of main function
