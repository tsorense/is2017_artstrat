function plot_grid( dat, fnum )
%
%    plot_grid( avi, fnum );
%

    dcr	 = 5;               % size of cross at displayed points
    cm	 = gray(255);
    avi	 = dat.vid.avi;
    grid = dat.vid.grid;

    figure; image(avi(fnum).cdata);
    colormap(cm);
    axis square;

    cx = grid.glot(1);      % place cross at glottis
    cy = grid.glot(2);
    line( [cx-dcr cx+dcr-1], [cy cy], [0 0], 'Color','b', 'Linewidth',grid.linewd );
    line( [cx cx], [cy-dcr cy+dcr-1], [0 0], 'Color','b', 'Linewidth',grid.linewd );
    
    cx = grid.or1(1);       % place cross at lingual origin
    cy = grid.or1(2);
    line( [cx-dcr cx+dcr-1], [cy cy], [0 0], 'Color','b', 'Linewidth',grid.linewd );
    line( [cx cx], [cy-dcr cy+dcr-1], [0 0], 'Color','b', 'Linewidth',grid.linewd );
    
    cx = grid.or2(1);       % place cross at alveolar origin
    cy = grid.or2(2);
    line( [cx-dcr cx+dcr-1], [cy cy], [0 0], 'Color','b', 'Linewidth',grid.linewd );
    line( [cx cx], [cy-dcr cy+dcr-1], [0 0], 'Color','b', 'Linewidth',grid.linewd );
    
    for gl = 1:grid.nlines	% draw line across tract at each grid interval
        grd = grid.ends(gl);
        txt = grid.txt(gl);
        hGL = line( grd.x, grd.y, [0 0], 'Color','b', 'Linewidth',grid.linewd );                
        set( hGL, 'UserData',gl, 'HitTest','on', 'ButtonDownFcn',@gridline_sel );
        if ~mod(gl,5)
            text( txt.x,txt.y, num2str(gl), 'Color','b', 'Linewidth',grid.linewd );
        end
    end %for


end %of main function
