function compare_tongues( dat,varargin )
%
%	compare_tongues( dat, lab1,...,labn, txt );
%
%   superimpose multiple plots of tongue postures for comparison
%	labels all taken from single data structure
%	tongue edge extracted from centre from of labeled sequence
%
%   eg. compare_tongue( zf1b,'asa','asha','zf' );
%

    palcol = [0.6 0.6 0.6];
    
    % fetch center frame from each labeled sequence
    nlabels	= nargin-2;
    for i = 1:nlabels
        label	= char(varargin{i});
        fint	= dat.seg.(label).fint;
        label_	= regexprep(char(label), '_', '][');
        lab{i}  = label_;
        fc(i)	= round(mean(fint));
    end
    
    % plot tract outline: centre frame of sequence
    hPLT = figure;	hold on;
    lwd	= 1;
    if ~isempty( dat.vt(fc(1)).pts )
        glot  = dat.vid.grid.glot;
        inner = [dat.vt(fc(1)).pts.lf];
        outer = [dat.vt(fc(1)).pts.rt];
        ibnd  = [glot inner];
        obnd  = [glot outer ibnd(end-1:end)];
        line( ibnd(1:2:end),ibnd(2:2:end), 'Color',palcol, 'Linewidth',lwd );
        line( obnd(1:2:end),obnd(2:2:end), 'Color',palcol, 'Linewidth',lwd );
    end
    
    % find range of lingual gridlines
    tgl = dat.vid.grid.tng;
    col	= {'r','b','g','y','k'};
    lwd	= 2;
    
    % plot lingual configuration at center frame of each labeled sequence
    for i = 1:nlabels
        if ~isempty( dat.vt(fc(i)).pts )
            tongue	= [dat.vt(fc(i)).pts(tgl).lf];
            h(i) = line( tongue(1:2:end),tongue(2:2:end), 'Color',col{i}, 'Linewidth',lwd );
        end
    end
    
    % annotate and tidy up plots
    title( varargin{nargin-1} );
    axis square; axis tight; axis off;
    set( gca, 'YDir','reverse');
    set( gcf, 'color','white' );
    legend( h,lab, 'Location','SouthWest' );

end %of main function
