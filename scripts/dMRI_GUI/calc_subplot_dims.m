function [l b w h] = calc_subplot_dims( r,c, iy,ix, bx,by, dx,dy )
    
% CALC_SUBPLOT_DIMS
% Calculate location and dimensions of subplot.
%
% Useage: [l b w h] = calc_subplot_dims( r,c, iy,ix, bx,by, dx,dy )
%
% 	r (int)     number of rows
% 	c (int)     number of cols
% 	iy (int)	y-index of subplot
% 	ix (int)	x-index of subplot
%
%   bx (0..1)	horizontal border
%	by (0..1)	vertical border
%	dx (0..1)	horizontal inter-plot spacing
%	dy (0..1)	vertical inter-plot spacing
%

    if ( max(iy) <= r ) && ( max(ix) <= c )
        
        wpl	= 1 - 2*bx;
        hpl	= 1 - 2*by;

        w	= wpl/c - dx;
        h	= hpl/r - dy;

        l	= bx + (ix(1)-1)  *(wpl/c) + dx/2;
        b	= by + (r-iy(end))*(hpl/r) + dy/2;

        if ( length(ix) > 1 )
            l2	= bx + (ix(end)-1)*(wpl/c) + dx/2;
            rt	= l2 + w;
            w	= rt - l;
        end

        if ( length(iy) > 1 )
            tp	= 1-(by + (iy(1)-1)*(hpl/r) + dy/2);
            h	= tp - b;
        end
        
    else
        fprintf('\n    Subplot indices must be less than or equal to number of rows and columns ...\n');
    end

end