function compare_tracts( dat, ff )
%
%    compare_tracts( dat, ff, mode );
%
%

    dcr	 = 5;               % size of cross at displayed points
    cm	 = gray(255);
    avi	 = dat.vid.avi;
    grid = dat.vid.grid;
    lwd	 = 1;
    
    ccycle = {'r','g','b','y','w'};

    figure;
    image(avi(ff(1)).cdata);
    colormap(cm);
    axis square; axis off;

    showgrid = 0;
    if (showgrid)
        cx = grid.glot(1);      % place cross at glottis
        cy = grid.glot(2);
        line( [cx-dcr cx+dcr-1], [cy cy], [0 0], 'Color','b', 'Linewidth',lwd );
        line( [cx cx], [cy-dcr cy+dcr-1], [0 0], 'Color','b', 'Linewidth',lwd );

        cx = grid.or1(1);       % place cross at lingual origin
        cy = grid.or1(2);
        line( [cx-dcr cx+dcr-1], [cy cy], [0 0], 'Color','b', 'Linewidth',lwd );
        line( [cx cx], [cy-dcr cy+dcr-1], [0 0], 'Color','b', 'Linewidth',lwd );

        cx = grid.or2(1);       % place cross at alveolar origin
        cy = grid.or2(2);
        line( [cx-dcr cx+dcr-1], [cy cy], [0 0], 'Color','b', 'Linewidth',lwd );
        line( [cx cx], [cy-dcr cy+dcr-1], [0 0], 'Color','b', 'Linewidth',lwd );
        
        for gl = 1:grid.nlines	% draw line across tract at each grid interval
            grd = grid.ends(gl);
            txt = grid.txt(gl);
            line( grd.x, grd.y, [0 0], 'Color','b', 'Linewidth',lwd );                
            if ~mod(gl,5)
                text( txt.x,txt.y, num2str(gl), 'Color','b', 'Linewidth',lwd );
            end
        end %for

    end
    
    cix = 1;
    for f = ff
        if ~isempty( dat.vt(f).pts )
            glot  = dat.vid.grid.glot;
            inner = [dat.vt(f).pts.lf];
            outer = [dat.vt(f).pts.rt];
            ibnd  = [glot inner];
            obnd  = [glot outer ibnd(end-1:end)];
            c     = char(ccycle(cix));
            line( ibnd(1:2:end),ibnd(2:2:end), 'Color',c, 'LineWidth',2 );
            line( obnd(1:2:end),obnd(2:2:end), 'Color',c, 'LineWidth',2 );
            cix	  = cix+1;
        end
    end

    
end %of main function
