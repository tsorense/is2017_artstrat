function plot_tongue( dat, label )
%
%	plot_tongue( dat, label );
%   
%   plot lingual movement in new figure; tongue edges extracted
%   from segmented sequence of frames defined by string <label> 
%   in data structure <dat>
%
%   eg. plot_tongue( zf1b,'asa' );
%

    % fetch range of frames defined by sequence label
    fi	= dat.seg.(label).fint;
    fc	= round(mean(fi));
    ff	= fi(1):fi(2);
    nf	= diff(fi) + 1;
    
    % plot tract outline: centre frame of sequence
    figure;
    hold on;
    lwd	= 1;
    if ~isempty( dat.vt(fc).pts )
        glot  = dat.vid.grid.glot;
        inner = [dat.vt(fc).pts.lf];
        outer = [dat.vt(fc).pts.rt];
        ibnd  = [glot inner];
        obnd  = [glot outer ibnd(end-1:end)];
        line( ibnd(1:2:end),ibnd(2:2:end), 'Color','w', 'Linewidth',lwd );
        line( obnd(1:2:end),obnd(2:2:end), 'Color','w', 'Linewidth',lwd );
    end
    
    % create gradient colormap same size as vector to be plotted
    cix     = 1;
    colspec	= 'autumn';
    cmap	= eval(['flipud( ' colspec '(' num2str(nf) '))']);

    % plot lingual movement over specified time interval
    lwd	= 2;
    tgl = dat.vid.grid.tng;     % find range of lingual gridlines
    for f = ff
        if ~isempty( dat.vt(f).pts )
            tongue	= [dat.vt(f).pts(tgl).lf];
            line( tongue(1:2:end),tongue(2:2:end), 'Color',cmap(cix,:), 'Linewidth',lwd );
        end
        cix = cix+1;
    end

    axis square; axis off;
    set(gca, 'YDir', 'reverse')

end %of main function
