function plot_tract_cr( dat, label, subj )
%
%	plot_tract_cr( avi, label, subj );
%
%   plot 1st half (constriction) and 2nd half (release )
%	of VCV frame sequence in side-by-side figures
%

    palcol = [0.6 0.6 0.6];
    
    % fetch range of frames defined by sequence label
    fi	= dat.seg.(label).fint;
    fc	= round(mean(fi));
    f1	= fi(1);
    f2	= fi(2);
    nc	= fc-f1+1;
    
    % calculate subplot placement params
    [l1 b1 w1 h1] = calc_subplot_dims( 1,2, 1,1, 0.05,0.2, 0,0 );
    [l2 b2 w2 h2] = calc_subplot_dims( 1,2, 1,2, 0.05,0.2, 0,0 );
    
    % plot tract outline: centre frame of sequence
    figure;	hold on;
    %lwd	= 1;
    %if ~isempty( dat.vt(fc).pts )
    %    glot  = dat.vid.grid.glot;
    %    inner = [dat.vt(fc).pts.lf];
    %    outer = [dat.vt(fc).pts.rt];
    %    ibnd  = [glot inner];
    %    obnd  = [glot outer ibnd(end-1:end)];
    %    subplot('position',[l1 b1 w1 h1]);
    %    line( ibnd(1:2:end),ibnd(2:2:end), 'Color',palcol, 'Linewidth',lwd );
    %    line( obnd(1:2:end),obnd(2:2:end), 'Color',palcol, 'Linewidth',lwd );
    %    subplot('position',[l2 b2 w2 h2]);
    %    line( ibnd(1:2:end),ibnd(2:2:end), 'Color',palcol, 'Linewidth',lwd );
    %    line( obnd(1:2:end),obnd(2:2:end), 'Color',palcol, 'Linewidth',lwd );
    %end
    
    % find range of lingual gridlines
    %tgl = dat.vid.grid.tng;
    cm2	= winter(nc);
    cm1	= flipud(cm2);
    lwd	= 2;
    
    % plot lingual closure sequence
    cix = 1;
    subplot('position',[l1 b1 w1 h1]); hold on;
    if (nargin>2)
        title(['[' label '] - (' subj ') - formation (f' num2str(f1) ':f' num2str(fc) ')']);
    else
        title(['[' label '] - formation (f' num2str(f1) ':f' num2str(fc) ')']);
    end
    for f = fi(1):fc
        if ~isempty( dat.vt(fc).pts )
            glot  = dat.vid.grid.glot;
            inner = [dat.vt(f).pts.lf];
            outer = [dat.vt(f).pts.rt];
            ibnd  = [glot inner];
            obnd  = [glot outer ibnd(end-1:end)];
            line( ibnd(1:2:end),ibnd(2:2:end), 'Color',cm1(cix,:), 'Linewidth',lwd );
            line( obnd(1:2:end),obnd(2:2:end), 'Color',cm1(cix,:), 'Linewidth',lwd );
        end
        cix = cix+1;
    end
    axis square; axis tight; axis off;
    set( gca, 'YDir','reverse');
    set( gcf, 'color','white' );
    
    % plot lingual release sequence
    cix = 1;
    subplot('position',[l2 b2 w2 h2]); hold on;
    if (nargin>2)
        title(['[' label '] - (' subj ') - release (f' num2str(fc) ':f' num2str(f2) ')']);
    else
        title(['[' label '] - release (f' num2str(fc) ':f' num2str(f2) ')']);
    end
    for f = fc:fi(2)
        if ~isempty( dat.vt(fc).pts )
            glot  = dat.vid.grid.glot;
            inner = [dat.vt(f).pts.lf];
            outer = [dat.vt(f).pts.rt];
            ibnd  = [glot inner];
            obnd  = [glot outer ibnd(end-1:end)];
            line( ibnd(1:2:end),ibnd(2:2:end), 'Color',cm2(cix,:), 'Linewidth',lwd );
            line( obnd(1:2:end),obnd(2:2:end), 'Color',cm2(cix,:), 'Linewidth',lwd );
        end
        cix = cix+1;
    end
    axis square; axis tight; axis off;
    set(gca, 'YDir','reverse');

end %of main function
