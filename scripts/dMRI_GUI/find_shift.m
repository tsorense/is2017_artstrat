 % find shift
 function [xshift, yshift]=find_shift(M1,M2,VARM,xsection1,ysection1,xsection2,ysection2) %takes in matrix and returns shift rows and columns 
        
        M1=double(M1(:,:,1));
        M2=double(M2(:,:,1));        
    
        output1 = dftregistration(fft2(M1(ysection1,xsection1)),fft2(M2(ysection1,xsection1)),10);
        output2 = dftregistration(fft2(M1(ysection2,xsection2)),fft2(M2(ysection2,xsection2)),10);
        a=output1(1)/output2(1);
        xshift=-round((output1(4)+a^2*output2(4))/(a^2+1)); %a positive shift is a shift down or right of M2 relative to M1
        yshift=-round((output1(3)+a^2*output2(3))/(a^2+1));
        
 end