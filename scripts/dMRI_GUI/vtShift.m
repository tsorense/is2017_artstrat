function [xs1,ys1,xs2,ys2]=vtShifta(M,VARM)    

    imsize=size(M,2);    
    an=1;
    xsection1a=zeros(10,2);
    ysection1a=zeros(10,2);
    xsection2a=zeros(10,2);
    ysection2a=zeros(10,2);
        
    for k=floor(1:(size(M,1)-1)/10:size(M,1))
        
        var1=mean(VARM,1);
        for i=1:1:imsize
            if var1(i)>50
                break;
            end
        end
        ysection1a(an,:)=[1,i-1+4];
        M1D=diff(M(k,i-1,:))*100/max(M(k,i-1,:));
        for j=1:1:imsize-1
            if abs(M1D(j))>3%20
                break;
            end
        end
        for i=imsize-1:-1:1
            if abs(M1D(i))>3%20
                break;
            end
        end
        xsection1a(an,:)=[j+1,i+1];        
    
        var2=mean(VARM,2);
        for j=imsize:-1:1
            if var2(j)>50
                break;
            end
        end
        xsection2a(an,:)=[j+1-4,imsize];        
        M2D=diff(M(k,:,j+1))*100/max(M(k,:,j+1));
        for i=1:1:imsize-1
            if abs(M2D(i))>3%20
                break;
            end
        end
        for j=imsize-5:-1:1
            if abs(M2D(j))>3%20
                break;
            end
        end
        ysection2a(an,:)=[i+1,j+1];
        
        an=an+1;
            
    end
        
    xsa1=median(xsection1a,1);
    ysa1=median(ysection1a,1);
	xsa2=median(xsection2a,1);
    ysa2=median(ysection2a,1);
    
    xs1=xsa1(1):1:xsa1(2);
    ys1=ysa1(1):1:ysa1(2);
    xs2=xsa2(1):1:xsa2(2);
    ys2=ysa2(1):1:ysa2(2);
    
end