% MAIN - discover constriction locations from the data, measure
% constriction degrees, and plot constriction degrees.
% 
% see also getTV, plotTV
% 
% Tanner Sorensen
% Signal Analysis and Interpretation Laboratory
% Feb. 14, 2017

% A. Scan 1
% measure task variables
getTV(configStruct,'rep1')

% plot task variable figure
plotTV(configStruct,'rep1')

% B. Scan 2
% measure task variables
getTV(configStruct,'rep2')

% plot task variable figure
plotTV(configStruct,'rep2')
