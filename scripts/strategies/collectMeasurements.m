function [ jawHannSum, lipHannSum, tngHannSum, velHannSum, dzHannSum, ...
    jawSum, lipSum, tngSum, velSum, dzSum] = collectMeasurements( configStruct, dataset, folder, colName, strategies )
%COLLECTMEASUREMENTS - collect articulatory strategy biomarkers for the
%right articulators in the right linguistic tasks
% 
% INPUT
%  Variable name: configStruct
%  Size: 1x1
%  Class: struct
%  Description: Fields correspond to constants and hyperparameters. 
%  Fields: 
%  - outPath: (string) path for saving MATLAB output
%  - aviPath: (string) path to the AVI files
%  - graphicsPath: (string) path to MATALB graphical output
%  - trackPath: (string) path to segmentation results
%  - manualAnnotationsPath: (string) path to manual annotations
%  - timestamps_file_name_<dataset>: (string) file name with path of 
%      timestamps file name for each data-set <dataset> of the analysis
%  - folders_<dataset>: (cell array) string folder names which belong to 
%      each data-set <dataset> of the analysis
%  - tasks: (cell array) string identifiers for different tasks
%  - FOV: (double) size of field of view in mm^2
%  - Npix: (double) number of pixels per row/column in the imaging plane
%  - framespersec_<dataset>: (double) frame rate of reconstructed real-time
%      magnetic resonance imaging videos in frames per second for each 
%      data-set <dataset> of the analysis
%  - ncl: (double array) entries are (i) the number of constriction 
%      locations at the hard and soft palate and (ii) the number of 
%      constriction locations at the hypopharynx (not including the 
%      nasopharynx).
%  - f: (double) hyperparameter which determines the percent of data used 
%      in locally weighted linear regression estimator of the jacobian; 
%      multiply f by 100 to obtain the percentage
%  - verbose: controls non-essential graphical and text output
%  
%  Variable name: dataset
%  Size: arbitrary
%  Class: char
%  Description: determines which data-set to analyze; picks out the
%  appropriate constants from configStruct.
% 
%  Variable name: folder
%  Size: arbitrary
%  Class: char
%  Description: determines which participant/scan of the data-set to 
%    analyze
% 
%  Variable name: colName
%  Size: arbitrary
%  Class: char
%  Description: determines the linguistic task (e.g., 'aia-pal-clo') which
%    is to be analyzed
%  
%  Variable name: strategies
%  Size: 1x1
%  Class: struct
%  Description: Struct with fields for each participant. Each field is 
%  itself a struct with the following fields.
%  - jaw: Nx6 array of double; entries are jaw contributions to change in
%  each of 6 constriction degrees (columns) in N real-time magnetic 
%  resonance imaging video frames (rows)
%  - lip: Nx6 array of double; entries are lip contributions to change in
%  each of 6 constriction degrees (columns) in N real-time magnetic 
%  resonance imaging video frames (rows)
%  - tng: Nx6 array of double; entries are tongue contributions to change 
%  in each of 6 constriction degrees (columns) in N real-time magnetic 
%  resonance imaging video frames (rows)
%  - tng: Nx6 array of double; entries are velum contributions to change 
%  in each of 6 constriction degrees (columns) in N real-time magnetic 
%  resonance imaging video frames (rows)
%  - dz: Nx6 array of double; entries are change in each of 6 constriction 
%  degrees (columns) in N real-time magnetic resonance imaging video frames
%  (rows)
%  - dw: Nx8 array of double; entries are change in 8 factor coefficients 
%  (columns) in N real-time magnetic resonance imaging video frames (rows)
%  - cl: (cell array of strings) identifier for the 6 places of 
%  articulation
% 


verbose = configStruct.verbose;
strategies = strategies.(folder);

jaw = strategies.jaw;
lip = strategies.lip;
tng = strategies.tng;
vel = strategies.vel;
dz = strategies.dz;

timestamps_file_name = configStruct.(sprintf('timestamps_file_name_%s',dataset));
outPath = configStruct.outPath;

% Set task variable.
load(fullfile(outPath,sprintf('tv_%s.mat',dataset)));
cl = cell(1,length(tv.(folder).tv));
for i=1:length(tv.(folder).tv)
    cl{i} = tv.(folder).tv{i}.cl;
end
if ~isempty(strfind(colName,'apa'))
    taskVar = find(cellfun(@(x) strcmp(x,'bilabial'), cl));
elseif ~isempty(strfind(colName,'ata'))
    taskVar = find(cellfun(@(x) strcmp(x,'alv'), cl));
elseif ~isempty(strfind(colName,'aia-pal'))
    taskVar = find(cellfun(@(x) strcmp(x,'pal'), cl));
elseif ~isempty(strfind(colName,'aka'))
    taskVar = find(cellfun(@(x) strcmp(x,'softpal'), cl));
elseif ~isempty(strfind(colName,'aia-phar'))
    taskVar = find(cellfun(@(x) strcmp(x,'pharL'), cl));
end

% Load the time-stamps and the associated filename and speaker IDs. 
[tab,txt] = xlsread(timestamps_file_name);
hdr = txt(1,:);
txt = txt(2:end,:);
nRow = size(tab,1);
rowIdx = cellfun(@(x) strcmp(x,strrep(folder,'participant_','')),txt(1:nRow,2));

% Convert start and stop time-points to frame number
startIdx = find(cellfun(@(x) strcmpi(x,[colName, '-start']),hdr))-2;
starts = tab(rowIdx,startIdx);
starts = starts(~isnan(starts));
endIdx = find(cellfun(@(x) strcmpi(x,[colName, '-end']),hdr))-2;
ends = tab(rowIdx,endIdx);
ends = ends(~isnan(ends));

% frame and file IDs for the track file frames.
load(fullfile(outPath,sprintf('contourdata_%s.mat',dataset)))
frames_trk = contourdata.(folder).VideoFrames;
files_trk = contourdata.(folder).File;
fl_trk = contourdata.(folder).fl;

filenames = find(cellfun(@(x) ~isempty(strfind(x,colName(1:3))), fl_trk));
nFile = length(filenames);

% Collect measurements by item.
jawHannSum = zeros(1,nFile);
lipHannSum = zeros(1,nFile);
tngHannSum = zeros(1,nFile);
velHannSum = zeros(1,nFile);
dzHannSum = zeros(1,nFile);
jawSum = zeros(1,nFile);
lipSum = zeros(1,nFile);
tngSum = zeros(1,nFile);
velSum = zeros(1,nFile);
dzSum = zeros(1,nFile);

if verbose, figure; end
for i=1:nFile
    if ~isnan(starts(i))
        idx = files_trk==filenames(i) & frames_trk >= starts(i) & frames_trk <= ends(i);
        
        jawHannSum(i) = sum(hann(sum(idx)).*jaw(idx,taskVar));
        lipHannSum(i) = sum(hann(sum(idx)).*lip(idx,taskVar));
        tngHannSum(i) = sum(hann(sum(idx)).*tng(idx,taskVar));
        velHannSum(i) = sum(hann(sum(idx)).*vel(idx,taskVar));
        dzHannSum(i) = sum(hann(sum(idx)).*dz(idx,taskVar));
        
        jawSum(i) = sum(jaw(idx,taskVar));
        lipSum(i) = sum(lip(idx,taskVar));
        tngSum(i) = sum(tng(idx,taskVar));
        velSum(i) = sum(vel(idx,taskVar));
        dzSum(i) = sum(dz(idx,taskVar));

        if verbose % then plot the contributions of jaw and lips.
            subplot(2,5,i)
            bar([cumsum(jaw(idx,taskVar)) cumsum(tng(idx,taskVar)) cumsum(lip(idx,taskVar))],'stacked')
            nPt = sum(idx);
            xlim([0 nPt+1]), set(gca,'XTick',[])
        end
    end
end

end

