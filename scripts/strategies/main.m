% MAIN - calculate articulatory strategies and save output files. 
% 
% Tanner Sorensen
% Signal Analysis and Interpretation Laboratory
% Feb. 14, 2017

% A. repeatability dataset
articulatory_strategies(configStruct,'rep1')

% B. repeatability dataset
articulatory_strategies(configStruct,'rep2')