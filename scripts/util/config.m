function configStruct = config
% CONFIG - set constants and parameters of the analysis
% 
% INPUT
%  none
% 
% FUNCTION OUTPUT:
%  Variable name: configStruct
%  Size: 1x1
%  Class: struct
%  Description: Fields correspond to constants and hyperparameters. 
%  Fields: 
%  - outPath: (string) path for saving MATLAB output
%  - aviPath: (string) path to the AVI files
%  - graphicsPath: (string) path to MATALB graphical output
%  - trackPath: (string) path to segmentation results
%  - manualAnnotationsPath: (string) path to manual annotations
%  - timestamps_file_name_<dataset>: (string) file name with path of 
%      timestamps file name for each data-set <dataset> of the analysis
%  - folders_<dataset>: (cell array) string folder names which belong to 
%      each data-set <dataset> of the analysis
%  - tasks: (cell array) string identifiers for different tasks
%  - FOV: (double) size of field of view in mm^2
%  - Npix: (double) number of pixels per row/column in the imaging plane
%  - framespersec_<dataset>: (double) frame rate of reconstructed real-time
%      magnetic resonance imaging videos in frames per second for each 
%      data-set <dataset> of the analysis
%  - ncl: (double array) entries are (i) the number of constriction 
%      locations at the hard and soft palate and (ii) the number of 
%      constriction locations at the hypopharynx (not including the 
%      nasopharynx).
%  - f: (double) hyperparameter which determines the percent of data used 
%      in locally weighted linear regression estimator of the jacobian; 
%      multiply f by 100 to obtain the percentage
%  - verbose: controls non-essential graphical and text output
% 
% SAVED OUTPUT: 
%  none
% 
% EXAMPLE USAGE: 
%  >> configStruct = config;
% 
% Tanner Sorensen
% Signal Analysis and Interpretation Laboratory
% Feb. 14, 2017

% paths
outPath = '/home/tsorense/spring2017/is2017_artstrat/mat';
aviPath = '/home/tsorense/spring2017/data';
graphicsPath = '/home/tsorense/spring2017/is2017_artstrat/graphics';
trackPath = '/home/tsorense/spring2017/is2017_artstrat/mat/segmentation_results';

manualAnnotationsPath = '/home/tsorense/spring2017/is2017_artstrat/manual_annotations';

% timestamps file name
timestamps_file_name_rep1 = '/home/tsorense/spring2017/is2017_artstrat/manual_annotations/timestamps_rep1.xlsx';
timestamps_file_name_rep2 = '/home/tsorense/spring2017/is2017_artstrat/manual_annotations/timestamps_rep2.xlsx';

% array constants
folders_rep1 = {'1f_rep1','2f_rep1','3f_rep1','4f_rep1',...
    '5m_rep1','6m_rep1','7m_rep1','8m_rep1'};
folders_rep2 = {'1f_rep2','2f_rep2','3f_rep2','4f_rep2',...
    '5m_rep2','6m_rep2','7m_rep2','8m_rep2'};
tasks = {'apa-clo','apa-rel','ata-clo','ata-rel','aka-clo','aka-rel',...
    'aia-pal-clo','aia-phar-clo'};

% fixed parameters
FOV = 200; % 200 mm^2 field of view 
Npix = 68; % 68^2 total pixels
%spatRes = FOV/Npix; % spatial resolution
framespersec_rep1 = 1/(2*0.006004);
framespersec_rep2 = 1/(2*0.006004); % compare to earlier 1/(7*6.164)*1000
ncl = [3 1];

% free parameters
f = 0.1;

% control printed output
verbose = false;

% make the struct object
configStruct = struct('outPath',outPath,'aviPath',aviPath,...
    'graphicsPath',graphicsPath,'trackPath',trackPath,...
    'timestamps_file_name_rep1',timestamps_file_name_rep1,...
    'timestamps_file_name_rep2',timestamps_file_name_rep2,...
    'manualAnnotationsPath',manualAnnotationsPath,...
    'folders_rep1',{folders_rep1},...
    'folders_rep2',{folders_rep2},...
    'tasks',{tasks},...
    'FOV',FOV,'Npix',Npix,...%'spatRes',spatRes,...
    'framespersec_rep1',framespersec_rep1,...
    'framespersec_rep2',framespersec_rep2,...
    'f',f,'ncl',ncl,'verbose',verbose);

end