function get_Ugfa(configStruct,dataset)
% GET_UGFA - extract factors of vocal tract shape the contours in the files
% contourdata_<dataset>.mat
% 
% INPUT:
%  Variable name: configStruct
%  Size: 1x1
%  Class: struct
%  Description: Fields correspond to constants and hyperparameters. 
%  Fields: 
%  - outPath: (string) path for saving MATLAB output
%  - aviPath: (string) path to the AVI files
%  - graphicsPath: (string) path to MATALB graphical output
%  - trackPath: (string) path to segmentation results
%  - manualAnnotationsPath: (string) path to manual annotations
%  - timestamps_file_name_<dataset>: (string) file name with path of 
%      timestamps file name for each data-set <dataset> of the analysis
%  - folders_<dataset>: (cell array) string folder names which belong to 
%      each data-set <dataset> of the analysis
%  - tasks: (cell array) string identifiers for different tasks
%  - FOV: (double) size of field of view in mm^2
%  - Npix: (double) number of pixels per row/column in the imaging plane
%  - framespersec_<dataset>: (double) frame rate of reconstructed real-time
%      magnetic resonance imaging videos in frames per second for each 
%      data-set <dataset> of the analysis
%  - ncl: (double array) entries are (i) the number of constriction 
%      locations at the hard and soft palate and (ii) the number of 
%      constriction locations at the hypopharynx (not including the 
%      nasopharynx).
%  - f: (double) hyperparameter which determines the percent of data used 
%      in locally weighted linear regression estimator of the jacobian; 
%      multiply f by 100 to obtain the percentage
%  - verbose: controls non-essential graphical and text output
%  
%  Variable name: dataset
%  Size: arbitrary
%  Class: char
%  Description: determines which data-set to analyze; picks out the
%  appropriate constants from configStruct.
% 
% FUNCTION OUTPUT:
%  none
% 
% SAVED OUTPUT: 
%  Path: configStruct.pathOut
%  File name: U_gfa_<dataset>.mat
%  Variable name: U_gfa 
%  Size: 1x1
%  Class: struct
%  Description: Struct with fields for each subject (field name is subject 
%    ID, e.g., 'at1_rep'). The fields are 400x8 matrices of type double.
%    The columns correspond to factors and the rows correspond to 
%    coordinates of the factors on the (X,Y)-plane. 
%    - Column 1 - jaw factor
%    - Columns 2-5 - tongue factors
%    - Column 6-7 - lip factors
%    - Column 8 - velum factor
% 
% Tanner Sorensen
% Signal Analysis and Interpretation Laboratory
% Apr. 14, 2017

load(fullfile(configStruct.outPath,sprintf('contourdata_%s.mat',dataset)))
graphicsPath = configStruct.graphicsPath;
folders = configStruct.(sprintf('folders_%s',dataset));

warning('off','stats:pca:ColRankDefX')
warning('off','MATLAB:hg:AutoSoftwareOpenGL')

fprintf('Performing factor analysis of %s dataset.\n[',dataset)
for h=1:length(folders)
    participant = sprintf('participant_%s',folders{h});
    U_gfa.(participant)=[];
    
    U_jaw = get_Ujaw(contourdata,participant);
    U_gfa.(participant)=cat(2,U_gfa.(participant),U_jaw(:,1));
    
    U_tng = get_Utng(contourdata,participant,U_jaw);
    U_gfa.(participant)=cat(2,U_gfa.(participant),U_tng(:,1:4));
    
    U_lip = get_Ulip(contourdata,participant,U_jaw);
    U_gfa.(participant)=cat(2,U_gfa.(participant),U_lip(:,1:2));
    
    U_vel = get_Uvel(contourdata,participant);
    U_gfa.(participant)=cat(2,U_gfa.(participant),U_vel(:,1));

    SectionsID=contourdata.(participant).SectionsID;

    D=[contourdata.(participant).X,contourdata.(participant).Y];

    [Dnorm,meandata] = zscore(D);

    figure
    U = U_gfa.(participant);

    for i=1:8

        subplot(2,4,i);

        DD = Dnorm*U(:,i)*pinv(U(:,i));

        plot_from_xy(meandata+2*std(DD),SectionsID(1,:),'b'); hold on;
        plot_from_xy(meandata-2*std(DD),SectionsID(1,:),'r'); hold on;

        plot_from_xy(meandata,SectionsID(1,:),'k');

        title(['Component ',num2str(i)]);
        axis equal; axis off;

    end;
    
    print(fullfile(graphicsPath,sprintf('%s_factors.pdf',participant)),'-dpdf');
    
    fprintf('-%s-',participant)
end
fprintf(']\n')

close all

warning('on','stats:pca:ColRankDefX')
warning('on','MATLAB:hg:AutoSoftwareOpenGL')

save(fullfile(configStruct.outPath,sprintf('U_gfa_%s.mat',dataset)), 'U_gfa');

end