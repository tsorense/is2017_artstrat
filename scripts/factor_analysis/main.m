% MAIN - perform factor analysis of the contours in the files
% contourdata_<dataset>.mat
% 
% Tanner Sorensen
% Signal Analysis and Interpretation Laboratory
% Feb. 16, 2017

% A. repeatability dataset (scan 1)
get_Ugfa(configStruct,'rep1')

% B. repeatability dataset (scan 2)
get_Ugfa(configStruct,'rep2')