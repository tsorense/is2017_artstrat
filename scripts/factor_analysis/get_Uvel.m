function U_vel = get_Uvel(contourdata_pca,folder)
% GET_UJAW - obtain the lip factors
% 
% INPUT:
%  Variable name: contourdata 
%  Size: 1x1
%  Class: struct
%  Description: Struct with fields for each subject (field name is subject 
%    ID, e.g., 'at1_rep'). The fields are structs with the following 
%    fields.
%  Fields: 
%  - X: X-coordinates of tissue-air boundaries in columns and time-samples 
%      in rows
%  - Y: Y-coordinates of tissue-air boundaries in columns and time-samples 
%      in rows
%  - File: file ID for each time-sample, note that this indexes the cell 
%      array of string file names in fl
%  - fl: cell array of string file names indexed by the entries of File
%  - SectionsID: array of numeric IDs for X- and Y-coordinates in the 
%      columns of the variables in fields X, Y; the correspondences are as 
%      follows: 01 Epiglottis; 02 Tongue; 03 Incisor; 04 Lower Lip; 05 Jaw;
%      06 Trachea; 07 Pharynx; 08 Upper Bound; 09 Left Bound; 10 Low Bound;
%      11 Palate; 12 Velum; 13 Nasal Cavity; 14 Nose; 15 Upper Lip
%  - Frames: frame number; 1 is first segmented video frame
%  - VideoFrames: frame number; 1 is first frame of avi video file
%  
%  Variable name: folder
%  Size: arbitrary
%  Class: char
%  Description: determines which participant/scan to analyze.
% 
% FUNCTION OUTPUT:
%  Variable name: U_vel
%  Size: 400x1
%  Class: double
%  Description: entries correspond to coordinates of the factors on the 
%    (X,Y)-plane. 
% 
% SAVED OUTPUT: 
%  none
% 
% Tanner Sorensen
% Signal Analysis and Interpretation Laboratory
% Apr. 14, 2017

% dataset
contourdata=contourdata_pca;
D=[contourdata.(folder).X,contourdata.(folder).Y];
[Dnorm,meandata] = zscore(D);

%% Principal components analysis
% subset data to velum
SectionsID=contourdata.(folder).SectionsID;
SecID2=[SectionsID,SectionsID];
vtsection=12;                             % velum
Dnorm_zero = Dnorm;
Dnorm_zero(:,~ismember(SecID2,vtsection))=0;

% principal components analysis of the velum
[U,~,~,~,varpercent] = pca(Dnorm_zero);
U_velraw = U;

% close all;
% 
% figure(1);
% 
% for i=1:6
%     
%     subplot(2,3,i);
%     
%     DD = Dnorm_zero*U(:,i)*pinv(U(:,i));
% 
%     plot_from_xy(meandata+2*std(DD),SectionsID(1,:),'b'); hold on;
%     plot_from_xy(meandata-2*std(DD),SectionsID(1,:),'r'); hold on;
%     
%     plot_from_xy(meandata,SectionsID(1,:),'k');
%     
%     title(['Component ',num2str(i),', Total Variance Explained: ',num2str(varpercent(i))]);
%     
%     axis equal; axis off;
%     
% end;

%% Guided factor analysis
% obtain data covariance matrix
n = size(D,1);
R = Dnorm_zero'*Dnorm_zero/n; % covariance matrix

% obtain velum factor
t1 = U_velraw(:,1); % first velum PC
v = t1'*R*t1;       % variance of the first velum PC
h1 = t1/sqrt(v);    % first velum PC with unit variance 
                    % (i.e., var(Dnorm*h1) approximately equals 1)
f1 = (h1'*R)';      % scale the first velum PC by data covariance
U_vel = f1;         % velum factor

% figure(2)
% 
% subplot(1,2,2)
% 
% DD = D*f1*pinv(f1);
% 
% plot_from_xy(meandata+2*std(DD),SectionsID(1,:),'b'); hold on;
% plot_from_xy(meandata-2*std(DD),SectionsID(1,:),'r'); hold on;
% 
% plot_from_xy(meandata,SectionsID(1,:),'k');
% axis equal; axis off;
% 
% subplot(1,2,1)
% 
% DD = Dnorm_zero*U(:,1)*pinv(U(:,1));
% 
% plot_from_xy(meandata+2*std(DD),SectionsID(1,:),'b'); hold on;
% plot_from_xy(meandata-2*std(DD),SectionsID(1,:),'r'); hold on;
% 
% plot_from_xy(meandata,SectionsID(1,:),'k');
% axis equal; axis off;