% RECONSTRUCT_CONTOUR_DATA - demo script which shows how to reconstruct the
% frames from the factors.
% 
% Tanner Sorensen
% Signal Analysis and Interpretation Laboratory
% Apr. 14, 2017

outPath = '/home/tsorense/spring2017/is2017_artstrat/mat';
load(fullfile(outPath,'contourdata_rep.mat'))
load(fullfile(outPath,'U_gfa_rep.mat'))

subj = 'participant_1f_rep1';
frameNo = 123;

% normalize data
xy = [contourdata.(subj).X, contourdata.(subj).Y];
[xy,meandata,sigma] = zscore(xy);
% mu = repmat(mean(xy),size(contourdata.(subj).X,1),1);
% xy = xy-mu;

% get weights
w = xy*U_gfa.(subj);

% approximate data
xy_hat = w*pinv(U_gfa.(subj));
xy_hat = xy_hat(frameNo,:).*sigma + meandata;

% original data
xy = xy(frameNo,:).*sigma + meandata;

% plot for a frame number of your choice
plot_from_xy(xy_hat,contourdata.(subj).SectionsID,'r'); hold on
plot_from_xy(xy,contourdata.(subj).SectionsID,'k'); hold off
